# Installing the package
To install the package, an entry must be added to your projects *composer.json*.

## Production
For production instances, the code should be deployed through VCS. This will ensure the package is kept up to date.

```
"repositories": [   
    {
        "type": "vcs",
        "url": "https://bitbucket.org/oddesseysolutions/oddessey-welcome-mail"
    }
]
``` 

## Development
For package development purposes, the package can be linked so that changes don't have to be deployed to a VCS.

```
"repositories": [   
    {
        "type": "path",
        "url": "</path/to/package>"
    }
]
```

## Requiring the package
To install the package, run the following command.

`composer require oddesseysolutions/welcome-mail`

# Installation
To start of, make sure your project uses Laravel authorization. If it doesn't, enable it by running the artisan command.

## Laravel 6.0 and above

```
composer require laravel/ui
php artisan ui vue --auth
npm install && npm run dev
```

## Laravel 5.8

```
php artisan make:auth
```

## Publish the view
The package uses a custom view for the password view. This view needs to be published by running the following command. Please note that this will overwrite any changes in your existing password reset view.

`php artisan vendor:publish --force --tag=oddesseysolutions-welcome-mail`

# Implementation
## Views
Replace the `ResetsPasswords` trait on the default `ResetPasswordController` provided by Laravel with the updated `CreateOrResetPasswords` trait

## Sending the mail
On the class that will be responsible for sending mails, add the `CanSendWelcomeMail` trait.

After adding this trait, the mail can be send by calling the following function.
```
$this->sendWelcomeMail($user);
```

By default, this will use the `name` and `email` fields of a user. If these need to be changed, you can override them by passing them in as parameters
```
$this->sendWelcomeMail($user, 'first_name');
```
or
```
$this->sendWelcomeMail($user, 'first_name', 'company_email');
```

# Using custom templates
If you don't want to use the default laravel mail template, you can use the `sendCustomWelcomeMail` function. This allows you to define a custom layout for your mail. Properties can be passed in through the thrird parameter. These will be made available to the view.

```
$this->sendCustomWelcomeMail($user, 'view_name', []);
```

## Available parameters
In your custom mail view you can use the following parameters:

| Parameter        | Explanation          
| :------------- |:-------------|
| firstName     | The first name of the registered user |
| passwordUrl     | The url at which the user can set it's password |
| expirationHours     | The time in which the link expires (in hours) |

## Prop templates
All parameters provided by the mail template can also be templated in the props provided in the props array. This is done by writing the propery name in upper case between square brackets. For example, if you need the `firstName` parameter, it is written as `[FIRST_NAME]`.

## Overriding subject
This function also allows for overwriting the subject. This can be done by providing it as a parameter

```
$this->sendCustomWelcomeMail($user, 'view_name', [], 'name', 'email', 'Amazing subject!');
```

# Set from address in environment
The default sender needs to be defined in your environment. Add the following keys

```
MAIL_FROM_ADDRESS="<email_address_here>"
MAIL_FROM_NAME="<sender_name_here>"
```