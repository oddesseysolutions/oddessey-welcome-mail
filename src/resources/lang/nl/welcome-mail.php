<?php

return [
    'subject' => 'Welkom bij :appName',
    'title' => 'Hoi :firstName',
    'body' => 'Er is een account voor je aangemaakt. Klik op onderstaande knop om je wachtwoord in te stellen!',
    'button_text' => 'Klik hier',
    'duration_notice' => 'Deze link is :expirationHours uur geldig. Na het verlopen van deze link kan je de wachtwoord vergeten functie gebruiken om je wachtwoord in te stellen.',
    'regards' => 'Met vriendelijke groet',
];