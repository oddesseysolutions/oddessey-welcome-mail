<?php

return [
    'subject' => 'Welcome to :appName',
    'title' => 'Hello :firstName',
    'body' => 'An account has been created for you. Press the button below to set a password for your account!',
    'button_text' => 'Click here',
    'duration_notice' => 'This link expires in :expirationHours hours. After expiration, you can use the password forgotten function to set your password.',
    'regards' => 'Kind regards',
];