@component('mail::message')

@lang('oddesseysolutions-welcomemail::welcome-mail.title', ['firstName' => $firstName]),<br><br>
@lang('oddesseysolutions-welcomemail::welcome-mail.body')

@component('mail::button', ['url' => $passwordUrl])
@lang('oddesseysolutions-welcomemail::welcome-mail.button_text')
@endcomponent

@lang('oddesseysolutions-welcomemail::welcome-mail.duration_notice', ['expirationHours' => $expirationHours])<br><br>
@lang('oddesseysolutions-welcomemail::welcome-mail.regards'),<br>{{ config('app.name') }}

@endcomponent
