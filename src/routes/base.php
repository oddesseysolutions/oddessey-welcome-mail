<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::group(['middleware' => ['web', 'guest']], function () {
    Route::get('/password/create/{token}', '\App\Http\Controllers\Auth\ResetPasswordController@showCreateForm')->name('password.create');
    Route::post('password/reset', '\App\Http\Controllers\Auth\ResetPasswordController@reset')->name('password.update');
});

// Route::group(['middleware' => ['web']], function () {
//     Route::get('/auth/start', 'OddesseySolutions\OAuth\Controllers\AuthController@startAuth')->name('auth.start');
//     Route::get('/auth/callback', 'OddesseySolutions\OAuth\Controllers\AuthController@callback')->name('auth.callback');
// });
