<?php

namespace OddesseySolutions\WelcomeMail\Traits;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;


trait CreateOrResetPasswords {
    use ResetsPasswords;

    public function showCreateForm(Request $request, $token = null)
    {
        return $this->getView($request, $token, 'Create Password');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return $this->getView($request, $token, 'Reset Password');
    }

    private function getView(Request $request, $token = null, $localization = 'Reset Password') {
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email, 'localization' => $localization]
        );
    }
}