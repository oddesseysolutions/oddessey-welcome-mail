<?php

namespace OddesseySolutions\WelcomeMail\Traits;

use Mail;

use OddesseySolutions\WelcomeMail\Mail\UserWelcome;

trait CanSendWelcomeMail {
    
    function sendWelcomeMail($user, $nameKey = 'name', $emailKey = 'email') {
        $mailable = $this->buildMailable($user, 'UserWelcome', $nameKey, $emailKey);
        Mail::to($user)->send($mailable);
    }

    function sendCustomWelcomeMail($user, $view, $props = null, $nameKey = 'name', $emailKey = 'email', $subject = null) {
        $mailable = $this->buildMailable($user, 'UserWelcomeCustom', $nameKey, $emailKey);
        $mailable->setViewName($view);
        $mailable->setSubject($subject);
        $mailable->setProps($props);
        Mail::to($user)->send($mailable);
    }

    private function buildMailable($user, $mailable, $nameKey, $emailKey) {
        $token = app('auth.password.broker')->createToken($user);
        
        $name = $user->$nameKey;
        $email = $user->$emailKey;
        $className = "OddesseySolutions\WelcomeMail\Mail\\" . $mailable;
        return new $className($name, $email, $token);
    }
}