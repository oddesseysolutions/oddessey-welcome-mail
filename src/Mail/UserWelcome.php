<?php

namespace OddesseySolutions\WelcomeMail\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserWelcome extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $email;
    protected $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $token)
    {
        $this->name = $name;
        $this->email = $email;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $expirationHours = $this->getExpirationHours();

        return $this->markdown('oddesseysolutions-welcomemail::mail.user.welcome')
            ->from(env('MAIL_FROM_ADDRESS'))
            ->subject(__('oddesseysolutions-welcomemail::welcome-mail.subject', ['appName' => env('APP_NAME')]))
            ->with([
                'firstName' => $this->name,
                'expirationHours' => $expirationHours,
                'passwordUrl' => route('password.create', ['token' => $this->token]) . '?email=' . urlencode($this->email)
            ]);
    }

    protected function getExpirationHours() {
        if(app()->version() >= 6) {
            return config('auth.password_timeout') / 3600;
        } else {
            return config('auth.passwords.users.expire') / 60;
        }
    }
}
