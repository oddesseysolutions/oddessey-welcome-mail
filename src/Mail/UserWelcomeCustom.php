<?php

namespace OddesseySolutions\WelcomeMail\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserWelcomeCustom extends UserWelcome
{
    use Queueable, SerializesModels;

    private $viewName;
    private $mailProps;
    private $mailSubject;

    private $replacementMap = [
        'FIRST_NAME' => 'firstName',
        'PASSWORD_URL' => 'passwordUrl',
        'EXPIRATION_HOURS' => 'expirationHours'
    ];

    public function setViewName($viewName) {
        $this->viewName = $viewName;
    }

    public function setSubject($subject) {
        $this->mailSubject = $subject;
    }

    public function setProps($props) {
        $this->mailProps = $props;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $expirationHours = $this->getExpirationHours();

        $subject = isset($this->mailSubject) ? $this->mailSubject : __('oddesseysolutions-welcomemail::welcome-mail.subject', ['appName' => env('APP_NAME')]);

        $mailData = [
            'firstName' => $this->name,
            'expirationHours' => $expirationHours,
            'passwordUrl' => route('password.create', ['token' => $this->token]) . '?email=' . urlencode($this->email)
        ];

        if(isset($this->mailProps)) {
            $mailProps = $this->replaceTemplateVariables($this->mailProps, $mailData);
            $mailData = array_merge($mailData, $mailProps);
        }

        return $this->view($this->viewName)
            ->from(env('MAIL_FROM_ADDRESS'))
            ->subject($subject)
            ->with($mailData);
    }

    private function replaceTemplateVariables($subject, $mailData) {
        foreach($subject as $index => $prop) {
            if(is_array($prop)) {
                $subject[$index] = $this->replaceTemplateVariables($prop, $mailData);
            } else {
                foreach($this->replacementMap as $replacementCandidate => $replacementKey) {
                    $replacementTarget = "[{$replacementCandidate}]";
                    if(strpos($prop, $replacementTarget) > -1) {
                        $subject[$index] = str_replace($replacementTarget, $mailData[$replacementKey], $prop);
                    }
                }
            }
        }

        return $subject;
    }
}
