<?php

namespace OddesseySolutions\WelcomeMail;

class OddesseyWelcomeMailServiceProvider extends \Illuminate\Support\ServiceProvider {
    public function boot() {
        $this->loadRoutesFrom(__DIR__.'/routes/base.php');
        $this->loadViewsFrom(__DIR__.'/views', 'oddesseysolutions-welcomemail');
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'oddesseysolutions-welcomemail');

        $this->publishes([
            __DIR__. '/views/auth/passwords'  => resource_path('views/auth/passwords'),
        ], 'oddesseysolutions-welcome-mail');
    }

    public function register() {

    }
}